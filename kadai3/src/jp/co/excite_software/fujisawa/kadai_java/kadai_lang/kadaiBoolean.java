package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class kadaiBoolean {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

//		Boolean t = true;
//		Boolean f = false;
		Boolean t = Boolean.TRUE;
		Boolean f = Boolean.FALSE;

		// booleanValue()
		Boolean a = t.booleanValue();
		Boolean b = f.booleanValue();

		System.out.println(a);
		System.out.println(b);

		// compare(boolean x, boolean y)
		int m = Boolean.compare(t, a);
		System.out.println(m);
		m = Boolean.compare(t, f);
		System.out.println(m);

		// compareTo(Boolean b)
		int n = a.compareTo(b);
		System.out.println(n);
		n = b.compareTo(f);
		System.out.println(n);

		// equals(Object obj)
		System.out.println(a.equals(b));
		System.out.println(a.equals(t));

		//hashCode()
		System.out.println(a.hashCode());
		System.out.println(t.hashCode());
		System.out.println(b.hashCode());

		// toString()
		String str = t.toString();
		System.out.println(str);

		//parseBoolean(String s)
		System.out.println(Boolean.parseBoolean(str));

		//toString(boolean b)
		System.out.println();

		//valueOf(boolean b)


	}

}
