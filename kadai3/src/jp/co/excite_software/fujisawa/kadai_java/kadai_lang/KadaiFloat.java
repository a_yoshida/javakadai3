package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiFloat {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		Float a = new Float(3);
		Float b = new Float(3.5);
		Float c = new Float("4.56789");
		// Float d = new Float("abc"); //NumberFormatException
		Float e = new Float(5.123456789123456789);
		String str = "1.23456789";


		// toString
		System.out.println(Float.toString(a));
		System.out.println(Float.toString(b));
		System.out.println(Float.toString(c));

		// toHexString
		System.out.println(Float.toHexString(a));
		System.out.println(Float.toHexString(b));
		System.out.println(Float.toHexString(c));
		System.out.println(Float.toHexString(e));

		//valueOf
		System.out.println(Float.valueOf(str));

		System.out.println(Float.valueOf(a));


		//parseFloat


	}

}
