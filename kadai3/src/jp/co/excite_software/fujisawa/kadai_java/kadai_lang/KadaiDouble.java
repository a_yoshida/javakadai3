package jp.co.excite_software.fujisawa.kadai_java.kadai_lang;

public class KadaiDouble {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		/* コンストラクタ */
		Double d1 = new Double(99.99999999);
		Double d2 = new Double("-88.888");

		/* メソッド */

		//toString()
		System.out.println(d1.toString());

		//toHexString()
		System.out.println(Double.toHexString(d1));
		System.out.println(Double.toHexString(d2));

		//valueOf
		String str1 = " 28.26 ";
		String str2 = "abc1";
		System.out.println(Double.valueOf(str1));
		//System.out.println(Double.valueOf(str2));

		//parseDouble
		System.out.println(Double.parseDouble(str1));
		//System.out.println(Double.parseDouble(str2));

		//isNaN

	}

}
